﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Components
{
    class DisableAterTime : MonoBehaviour
    {
        [SerializeField]
        private float time = 3f;

        private void OnEnable()
        {
            StartCoroutine(Disable());
        }

        IEnumerator Disable()
        {
            yield return new WaitForSeconds(time);
            gameObject.SetActive(false);
        }
    }
}
