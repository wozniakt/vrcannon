﻿using UnityEngine;

namespace Assets.Scripts.Components
{
    class DistanceMeasurer:MonoBehaviour
    {
        public float GetDistance(Transform transformA, Transform transformB)
        {
            float distance =Mathf.Abs(Vector3.Distance(transformA.position,transformB.position));
            return distance;
        }
     }
}
