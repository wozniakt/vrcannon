﻿using UnityEngine;

namespace Assets.Scripts.Components
{
    class LineDisplayer : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer lineRenderer;
        private Transform transformA, transformB;

        bool visible;
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                lineRenderer.enabled = visible;
                visible = value;

            }
        }

        public void ShowLine(Transform tA, Transform tB)
        {
            transformA = tA;
            transformB = tB;
            Visible = true;
        }

        public void HideLine()
        {
            Visible = false;
        }

        private void Update()
        {
            if (Visible)
            {
                lineRenderer.SetPosition(0, transformA.position);
                lineRenderer.SetPosition(1, transformB.position);
            }

        }
    }
}
