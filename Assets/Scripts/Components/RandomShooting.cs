﻿using UnityEngine;

namespace Assets.Scripts.Components
{
    class RandomShooting : MonoBehaviour
    {
        [SerializeField]
        private Transform cannon;
        [SerializeField]
        private float forceModMin, forceModMax;
        [SerializeField]
        private Transform barrelEnd;

        public void ShootWithRandomForce(Vector3 euler, GameObject bulletPrefab, GameObject explosionPrefab)
        {
            MakeExplosion(explosionPrefab);
            GameObject bullet = PrepareBullet(bulletPrefab,euler);
           
            float forceMod = Random.Range(forceModMin, forceModMax);
            Vector3 force = bullet.transform.forward;
            force = new Vector3(force.x, 0, force.z);
            bullet.GetComponent<Rigidbody>().AddForce(force * forceMod);
        }

        GameObject PrepareBullet(GameObject prefab, Vector3 euler)
        {
            GameObject bullet = PoolManager.Instance.SpawnGoInstance(prefab);
            bullet.transform.position = barrelEnd.transform.position;
            bullet.SetActive(true);
            bullet.transform.eulerAngles = euler;
            return bullet;
        }

        void MakeExplosion(GameObject prefab)
        {
            GameObject explosion = PoolManager.Instance.SpawnGoInstance(prefab);
            explosion.transform.position = barrelEnd.transform.position;
            explosion.SetActive(true);
        }
    }
}
