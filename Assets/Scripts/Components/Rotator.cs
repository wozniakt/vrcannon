﻿using UnityEngine;

namespace Assets.Scripts.Components
{
    class Rotator : MonoBehaviour
    {
        public bool Rotating { get; set; }
        [SerializeField]
        private Transform cannonBarrel;
        [SerializeField]
        private float rotationSpeed;
        private float euler;

        public void StartRotation()
        {
            euler = cannonBarrel.eulerAngles.y;
            Rotating = true;
        }

        public void StopRotation()
        {
            Rotating = false;
        }

        public Vector3 GetCannonBarrelEuler()
        {
            return cannonBarrel.eulerAngles;
        }

        private void Update()
        {
            if (!Rotating)
                return;
            euler += Time.deltaTime * rotationSpeed;
            cannonBarrel.eulerAngles = new Vector3(cannonBarrel.eulerAngles.x, euler, cannonBarrel.eulerAngles.z);
        }
    }
}
