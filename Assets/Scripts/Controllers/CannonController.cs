﻿using Assets.Scripts.Components;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    class CannonController : MonoBehaviour
    {
        [SerializeField]
        private GameObject bulletPrefab;
        [SerializeField]
        private RandomShooting randomShooting;
        [SerializeField]
        private Rotator rotator;
        [SerializeField]
        private GameObject explosionPrefab;

        private void Start()
        {
            PoolManager.Instance.CreateGoInstances(10, bulletPrefab);
        }

        public void Shoot(Vector3 euler)
        {
            randomShooting.ShootWithRandomForce(euler, bulletPrefab, explosionPrefab);
        }

        public void StartRotate()
        {
            rotator.StartRotation();
        }

        public void StopRotate()
        {
            rotator.StopRotation();
        }
    }
}
