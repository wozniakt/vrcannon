﻿using Assets.Scripts.Components;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Controllers
{
    class GazeController:MonoBehaviour
    {
        [SerializeField]
        private GvrReticlePointer gvrReticlePointer;
        [SerializeField]
        private MeasureController measureController;
        private RaycastResult raycastResult;
        [SerializeField]
        private float delay =3f, delayRotate=1f;
        [SerializeField]
        private float timer;
        private CannonController gunController;

        private void Update()
        {
            raycastResult = gvrReticlePointer.CurrentRaycastResult;
            if (raycastResult.isValid == false || raycastResult.isValid == false || (!raycastResult.gameObject.CompareTag("Gun") && !raycastResult.gameObject.CompareTag("Bullet")))
            {
                if (gunController)
                    gunController.StopRotate();
                timer = 0;
                measureController.HideDistance();
                return;
            }
            
            if (raycastResult.gameObject.CompareTag("Gun") )
            {
                if (timer > delayRotate)
                {
                    gunController = raycastResult.gameObject.GetComponent<CannonController>();
                    gunController.StartRotate();
                }
                measureController.HideDistance();
                timer += Time.deltaTime;

                if (timer>delay)
                {
                    Vector3 euler= raycastResult.gameObject.GetComponent<Rotator>().GetCannonBarrelEuler();
                    raycastResult.gameObject.GetComponent<CannonController>().Shoot(euler);
                    gunController.StopRotate();
                    timer = 0;
                }
                return;
            }

            if (raycastResult.gameObject.CompareTag("Bullet")  )
            {
                timer += Time.deltaTime;
                if (timer > delay)
                    measureController.ShowDistance(raycastResult.gameObject.transform);
            }
        }
    }
}
