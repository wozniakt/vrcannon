﻿using Assets.Scripts.Components;
using System;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    class MeasureController : MonoBehaviour
    {
        public event Action<float, Transform> OnDistanceShowed = delegate { };
        public event Action OnDistanceHidden = delegate { };

        [SerializeField]
        private DistanceMeasurer distanceMeasurer;
        [SerializeField]
        private LineDisplayer lineDisplayer;
        [SerializeField]
        private Transform cannonCenter;
        
        public void ShowDistance(Transform bullet)
        {
            float distance = distanceMeasurer.GetDistance(bullet, cannonCenter);
            OnDistanceShowed.Invoke(distance, bullet);
            lineDisplayer.ShowLine(cannonCenter, bullet);
        }

        public void HideDistance()
        {
            OnDistanceHidden.Invoke();
            lineDisplayer.HideLine();
        }
    }
}
