﻿using System.Collections.Generic;
using UnityEngine;


public class PoolManager : MonoBehaviour
{
    public static PoolManager Instance;

    [SerializeField]
    private int pooledAmount = 10;
    private GameObject goInstancesPool;
    private List<GameObject> poolList = new List<GameObject>();

    private void Awake()
    {
        goInstancesPool = new GameObject("PoolObjects");
        goInstancesPool.transform.SetParent(this.transform);
        if (Instance == null)
            Instance = this;
    }

    public void ResetPooledList()
    {
        poolList = new List<GameObject>();
        Destroy(goInstancesPool);
        goInstancesPool = new GameObject();
        goInstancesPool.transform.SetParent(this.transform);
    }

    public void CreateGoInstances(int count, GameObject GoInstancePrefab)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = (GameObject)Instantiate(GoInstancePrefab);

            obj.SetActive(false);
            obj.name = GoInstancePrefab.name.ToString();
            poolList.Add(obj);
            obj.transform.SetParent(goInstancesPool.transform);
        }

    }

    public GameObject SpawnGoInstance(GameObject GoInstancePrefab)
    {
        foreach (GameObject item in poolList)
        {
            if (item.name == GoInstancePrefab.name.ToString())
            {
                if (!item.activeInHierarchy)
                {
                    return item;
                }
            }
        }

        GameObject obj = (GameObject)Instantiate(GoInstancePrefab);
        obj.name = GoInstancePrefab.name.ToString();
        poolList.Add(obj);
        obj.transform.SetParent(goInstancesPool.transform);
        return obj;

    }

}