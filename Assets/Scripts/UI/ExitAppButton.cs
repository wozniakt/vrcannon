﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    class ExitAppButton:MonoBehaviour
    {
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}
