﻿using Assets.Scripts.Controllers;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    class UIDistanceText:MonoBehaviour
    {
        [SerializeField]
        private MeasureController measureController;
        [SerializeField]
        private GameObject canvasMeasure;
        [SerializeField]
        private TextMeshProUGUI textDistance1;
        [SerializeField]
        Transform player;
        [SerializeField]
        float canvasOffsetY=1;
        void Start()
        {
            measureController.OnDistanceShowed += OnDistanceShowed;
            measureController.OnDistanceHidden += OnDistanceHidden;   
        }

        private void OnDistanceHidden()
        {
            canvasMeasure.SetActive(false);
        }

        private void OnDistanceShowed(float distance,Transform target)
        {
            canvasMeasure.SetActive(true);
       
            canvasMeasure.transform.position = new Vector3(target.position.x, target.position.y+ canvasOffsetY, target.position.z);
            textDistance1.text = String.Format("Distance: {0}", distance.ToString());
            canvasMeasure.transform.LookAt(player);
        }
    }
}
