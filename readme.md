# VR Cannon

## Description
Gamelike VR application. 
When user looks at the cannon, it starts to rotate (after 1 second) and after another 2 seconds it shoots a bullet with random force.
When user looks at bullet, after 3 seconds, distance is shown.

## Assets
- [Unity 2018.2.13f1](https://unity3d.com/get-unity/download)
- [GVR SDK for Unity v1.190.1](https://github.com/googlevr/gvr-unity-sdk/releases/tag/v1.190.1)
- [Cartoon FX Free](https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565)
- [Battle Cannon](https://assetstore.unity.com/packages/3d/environments/fantasy/battle-cannon-70589)
- [Shader FlippingNormals](https://gist.github.com/AdrianaVecc/20ae99182d89848086e95cbb6ed523e2#file-flippingnormals-shader)
- [Music: opengameart.org](https://opengameart.org/content/space-music-out-there)
- [Sound: opengameart.org](https://opengameart.org/content/25-cc0-bang-firework-sfx)